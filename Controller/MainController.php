<?php

namespace Pl\StripeBundle\Controller;


use Pl\StripeBundle\Interfaces\SripeEventCallbackManagerInterface;
use Stripe\Error\InvalidRequest;
use Stripe\Event;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\Dumper\FileDumper;
use Symfony\Component\VarDumper\VarDumper;

class MainController extends Controller
{

	public function events(Request $request)
	{
		\Stripe\Stripe::setApiKey($this->getParameter("pl_stripe.stripe_api_key"));
		\Stripe\Stripe::setApiVersion("2020-08-27;customer_balance_payment_method_beta=v2");
		$id = null;
		if($request->get("data")){
			$id = $request->get("id");
		}
		elseif($data = json_decode(@file_get_contents("php://input"))){
			$id = $data->id;
		}


		$payload = @file_get_contents('php://input');
		$sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
		$event = null;

		if($id !== null){
			$this->get("stripe_logger")->debug("Requête recue, event id : " . $id);

			/** @var SripeEventCallbackManagerInterface $callbackManager */
			if($this->container->hasParameter("pl_stripe.event_callback_service")){
				$serviceName = $this->getParameter("pl_stripe.event_callback_service");
				if($callbackManager = $this->get($serviceName)){
					try{
						if($request->get("account")){
							$endpoint_secret = getenv("STRIPE_WEBHOOK_CONNECT_SECRET");
						}
						else{
							$endpoint_secret = getenv("STRIPE_WEBHOOK_SECRET");
						}

						$event = \Stripe\Webhook::constructEvent(
							$payload, $sig_header, $endpoint_secret
						);

						$this->get("stripe_logger")->debug($event->type);
						$callbackManager->handleEvent($event);
					}
					catch(\Exception $e){
						if(!preg_match('#exists in test mode#', $e->getMessage())){
							$this->get("stripe_logger")->err(sprintf("Erreur : %s", $e->getMessage()));
							return new Response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
						}
						return new Response($e->getMessage());
					}
					return new Response("OK");
				}
			}

			$this->get("stripe_logger")->err("Erreur, aucune service pour gérer les events");
			return new Response("Erreur, aucune service pour gérer les events", Response::HTTP_INTERNAL_SERVER_ERROR);
		}
		$this->get("stripe_logger")->err("Requête invalide reçue sur le webhook");
		return new Response("Erreur, requête invalide", Response::HTTP_BAD_REQUEST);
	}

}


