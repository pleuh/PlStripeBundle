<?php

namespace Pl\StripeBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder
            ->root('pl_stripe')
            ->children()
            ->scalarNode('stripe_api_key')->end()
            ->scalarNode('stripe_api_pk')->end()
            ->scalarNode('event_callback_service')->end()
            ->end()
            ;
        return $treeBuilder;
    }
}
