<?php

//4242424242424242 visa
//4012888888881881 visa
//4000000000003063 3dsecure
//4000000000000002 card fail



namespace Pl\StripeBundle\Manager;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Pl\StripeBundle\Interfaces\StripeAccountUserInterface;
use Pl\StripeBundle\Interfaces\StripeUserInterface;
use Pl\StripeBundle\Interfaces\StripeVirementInterface;
use Stripe\Card;
use Stripe\Charge;
use Stripe\Error\InvalidRequest;
use Stripe\PaymentIntent;
use Stripe\Source;
use Stripe\Subscription;

/**
 * Class StripeManager
 * @package Pl\StripeBundle\Manager
 * @property EntityManager $em
 */
class StripeManager{
	private $em;

	/**
	 * StripeManager constructor.
	 * @param string $stripeKey
	 * @param EntityManagerInterface $em
	 */
	public function __construct($stripeKey, EntityManagerInterface $em){
		\Stripe\Stripe::setApiKey($stripeKey);
		\Stripe\Stripe::setApiVersion("2020-08-27;customer_balance_payment_method_beta=v2");
		$this->em = $em;
	}


	/**
	 * @param StripeUserInterface $user
	 * @return \Stripe\Customer
	 */
	public function tryGetCustomer(StripeUserInterface $user){
		if(!$user->getStripeId()){
			$customer = $this->createCustomer($user);
		} else{
			$customer = \Stripe\Customer::retrieve($user->getStripeId());
		}

		return $customer;
	}


	/**
	 * @param StripeUserInterface $user
	 * @return \Stripe\Customer
	 */
	public function createCustomer(StripeUserInterface $user, $bFlush = true, $otherOptions =[]){
		$data = array_merge([
			"email" => $user->getEmail(),
		], $otherOptions);

		$customer = \Stripe\Customer::create($data);
		$user->setStripeId($customer->id);
		$this->em->persist($user);
		if($bFlush){
			$this->em->flush();
		}

		return $customer;
	}

	/**
	 * @param $id
	 * @return null|\Stripe\Plan
	 */
	public function getPlan($id){
		try{
			return \Stripe\Plan::retrieve($id);
		} catch(InvalidRequest $e){
			if(preg_match('#No such plan#', $e->getMessage())){
				return null;
			}

			return null;
		}
	}

	public function createPlan($options){
		$plan = \Stripe\Plan::create($options);
		return $plan;
	}

	/**
	 * @param StripeUserInterface $user
	 * @param $planId
	 * @return \Stripe\Subscription
	 */
	public function subscribeToPlan(StripeUserInterface $user, $planId, $options = []){

		$data = array_merge([
			"customer" => $user->getStripeId(),
			"plan" => $planId,
		], $options);

		return \Stripe\Subscription::create($data);
	}

	/**
	 * @param StripeUserInterface $user
	 * @param $items
	 * @return \Stripe\Subscription
	 */
	public function subscribeToPlans(StripeUserInterface $user, $items, $metaData = null){
		$infos = [
			"customer" => $user->getStripeId(),
			"items" => $items,
		];

		if($metaData){
			$infos["metadata"] = $metaData;
		}

		return \Stripe\Subscription::create($infos);
	}

	/**
	 * @param $sourceId
	 * @return \Stripe\Source
	 */
	public function getSource($sourceId){
		return \Stripe\Source::retrieve($sourceId);
	}

	/**
	 * @param $subscriptionId
	 * @param $items
	 * @return \Stripe\Subscription
	 */
	public function updateSubscription($subscriptionId, $items){
		return \Stripe\Subscription::update($subscriptionId, ["items" => $items]);
	}

	/**
	 * @param $subscriptionId
	 * @param $items
	 * @return \Stripe\Subscription
	 */
	public function cancelSubscription($subscriptionId){
		$subscription = \Stripe\Subscription::retrieve($subscriptionId);
		return $subscription->cancel();
	}

	/**
	 * @param StripeUserInterface $user
	 * @return \Stripe\Collection|Subscription[]
	 */
	public function getSubscription($subscriptionId){
		return \Stripe\Subscription::retrieve($subscriptionId);
	}

	/**
	 * @param StripeUserInterface $user
	 * @return \Stripe\Collection|Subscription[]
	 */
	public function getSubscriptions(StripeUserInterface $user){
		return $this->tryGetCustomer($user)->subscriptions;
	}

	/**
	 * @param $source
	 * @param StripeUserInterface $user
	 */
	public function ajouterCarte($source, StripeUserInterface $user, $bFlush = true){
		if(!$user->getStripeId()){
			$this->createCustomer($user, $bFlush);
		}

		\Stripe\Customer::update($user->getStripeId(), [
			"source" => $source,
		]);
	}

	/**
	 * @param StripeUserInterface $user
	 * @return Source[]|Card[]
	 */
	public function getAllSources(StripeUserInterface $user){
		$sources = [];
		foreach($this->tryGetCustomer($user)->sources->all()->autoPagingIterator() as $source){
			if(get_class($source) == Card::class || $source->type == "card"){
				$sources[] = $source;
			}
		}
		return $sources;
	}

	/**
	 * @param StripeUserInterface $user
	 * @param $montant
	 * @param null $metaData
	 * @return \Stripe\Charge
	 */
	public function charger(StripeUserInterface $user, $montant, $options = [], $source = null, $destAccount = null){
		$infos = [
			"amount" => $montant,
			"currency" => "eur",
			"customer" => $user->getStripeId(),
		];
		$infos = array_merge($infos, $options);

		if($source){
			$infos["source"] = $source;
		}

		if($destAccount){
			$infos["transfer_data"] = ["destination" => $destAccount];
		}

		return \Stripe\Charge::create($infos);
	}

	/**
	 * @param StripeUserInterface $user
	 * @param $montant
	 * @param null $metaData
	 * @return \Stripe\Charge
	 */
	public function chargerAccount(StripeAccountUserInterface $account, $montant, $options = []){
		$infos = [
			"amount" => $montant,
			"currency" => "eur",
			"source" => $account->getStripeAccountId(),
		];
		$infos = array_merge($infos, $options);

		return \Stripe\Charge::create($infos);
	}






	/**
	 * @param StripeVirementInterface $virement
	 * @return StripeVirementInterface|void
	 */
	public function createVirement(StripeVirementInterface $virement){
		if($virement->getStripeSourceId()){
			return $virement;
		}
		$source = \Stripe\Source::create([
			"type" => "sepa_credit_transfer",
			"currency" => "eur",
			"owner" => [
				"name" => $virement->getFullName(),
				"email" => $virement->getEmail(),
			],
		]);


		$virement->setStripeIban($source->sepa_credit_transfer->iban);
		$virement->setStripeSourceId($source->id);
		return $virement;
	}


	/**
	 * @param $stripeChargeId
	 * @param array $options
	 * @return \Stripe\Refund
	 */
	public function refundsChargeStripe($stripeChargeId, $options = []){
		$infos = [
			"charge" => $stripeChargeId,
		];
		$infos = array_merge($infos, $options);

		$refund = \Stripe\Refund::create($infos);
		return $refund;
	}

	/**
	 * @param $amount
	 * @param $destination
	 * @param array $options
	 * @return \Stripe\Transfer
	 */
	public function transfert($amount, $destination, $options = []){
		$infos = [
			"amount" => $amount,
			"currency" => "eur",
			"destination" => $destination,
		];
		$infos = array_merge($infos, $options);

		$transfert = \Stripe\Transfer::create($infos);
		return $transfert;
	}

	public function getBalance(){
		return \Stripe\Balance::retrieve();
	}

	/**
	 * @param $amount
	 * @param array $options
	 * @return \Stripe\Payout
	 */
	public function payout($amount, $options = []){
		$infos = [
			'amount' => $amount,
			'currency' => 'eur',
		];
		$infos = array_merge($infos, $options);

		$payout = \Stripe\Payout::create($infos);
		return $payout;
	}

	/**
	 * @param int $amount
	 * @return PaymentIntent
	 */
	public function createPaiementIntent(int $amount, array $options = []) : PaymentIntent{
		$data = array_merge([
			'amount' => $amount,
			'currency' => 'eur',
		], $options);

		$intent = \Stripe\PaymentIntent::create($data);
		return $intent;
	}

	/**
	 * @param string $id
	 * @param PaymentIntent
	 */
	public function updatePaiementIntent(string $id, int $amount) : PaymentIntent{
		$intent = \Stripe\PaymentIntent::update(
			$id, ['amount' => $amount]
		);
		return $intent;
	}

	/**
	 * @param string $intentId
	 * @return PaymentIntent
	 */
	public function getPaymentIntent(string $intentId) : PaymentIntent{
		return \Stripe\PaymentIntent::retrieve($intentId);
	}

	/**
	 * @param string $intentId
	 * @throws \Stripe\Exception\ApiErrorException
	 */
	public function cancelIntention(string $intentId){
		$intent = $this->getPaymentIntent($intentId);
		$intent->cancel();

	}


	public function getDestinationPaymentTransfertFromCharge(Charge $charge) : ?string{
		if($charge->transfer){
			$transfert = \Stripe\Transfer::retrieve($charge->transfer);
			if($transfert->destination_payment){
				return $transfert->destination_payment;
			}
		}
		return null;
	}

}
