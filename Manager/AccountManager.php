<?php


namespace Pl\StripeBundle\Manager;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Pl\StripeBundle\Interfaces\StripeAccountUserInterface;
use Stripe\Account;

/**
 * Class AccountManager
 * @package Pl\StripeBundle\Manager
 * @property EntityManager $em
 */
class AccountManager
{
	private $em;

	public function __construct($stripeKey, EntityManagerInterface $em){
		\Stripe\Stripe::setApiKey($stripeKey);
		$this->em = $em;
	}


	/**
	 * @param StripeAccountUserInterface $accountUserInterface
	 * @param string $token
	 * @param string $externalAccountToken
	 * @param bool $bFlush
	 * @throws \Doctrine\ORM\ORMException
	 * @throws \Doctrine\ORM\OptimisticLockException
	 */
	public function storeUpdateAccount(
		StripeAccountUserInterface $accountUserInterface,
		string $accountToken,
		string $externalAccountToken,
		string $personToken = null,
		$bFlush = true,
		string $productDescription,
		string $country = null
	): Account{
		if(!$accountUserInterface->getStripeAccountId()){
		 
			$data = [
				"account_token" => $accountToken,
				"type" => "custom",
				'requested_capabilities' => $country === "US" ? ['transfers', 'card_payments'] : ['transfers'],
				"settings" => [
					"payouts" => [
						"schedule" => ["interval" => "manual"]
					]
				],
				"business_profile" => [
					"product_description" =>  $productDescription,
//					"url" =>  "https://www.tudigo.co",
				]
			];
			if($country){
				$data["country"] = $country;
			}
			$account = \Stripe\Account::create($data);
			$accountUserInterface->setStripeAccountId($account->id);
			$this->em->persist($accountUserInterface);
			if($bFlush){
				$this->em->flush();
			}

			if($personToken){
				foreach(explode(",", $personToken) as $indPersonToken){
					$person = \Stripe\Account::createPerson($accountUserInterface->getStripeAccountId(), [
						"person_token" => $indPersonToken,
					]);
				}


			}

			$extAccount = \Stripe\Account::createExternalAccount($accountUserInterface->getStripeAccountId(), [
				"external_account" => $externalAccountToken,
			]);

//			$account2 = \Stripe\Account::update($accountUserInterface->getStripeAccountId(), [
//				"company" => [
//					"directors_provided" => true,
//					"executives_provided" => true,
//				],
//			]);


		}
		else{
			$account = \Stripe\Account::update($accountUserInterface->getStripeAccountId(), [
				"account_token" => $accountToken,
				"settings" => [
					"payouts" => [
						"schedule" => ["interval" => "manual"]
					]
				],
			]);
            
            if($personToken && $account->business_type == "individual"){
                $person =$account->persons()->first();
                $tokens = explode(",", $personToken);
                $person = \Stripe\Account::updatePerson($accountUserInterface->getStripeAccountId(), $person["id"],  [
                    "person_token" => $tokens[0],
                ]);
            }
//			if($personToken){
//				$persons =$account->persons();
//				$person = \Stripe\Account::updatePerson($accountUserInterface->getStripeAccountId(), $persons[0]->id,  [
//					"person_token" => $personToken,
//				]);
//			}

//			if(in_array("legal_entity.additional_owners", $account->verification->fields_needed)){
//				$account->legal_entity->additional_owners = null;
//				$account->save();
//			}
		}

		return $account;
	}

	/**
	 * @param StripeAccountUserInterface $accountInterface
	 */
	public function retrieveStripeAccount(StripeAccountUserInterface $accountInterface){
		if(!$accountInterface->getStripeAccountId()){
			return null;
		}

		return \Stripe\Account::retrieve($accountInterface->getStripeAccountId());
	}


	/**
	 * @param StripeAccountUserInterface $accountInterface
	 * @return null|\Stripe\Balance
	 */
	public function retrieveBalanceFromAccount(StripeAccountUserInterface $accountInterface){
		if(!$accountInterface->getStripeAccountId()){
			return null;
		}
		return \Stripe\Balance::retrieve(["stripe_account" => $accountInterface->getStripeAccountId()]);
	}

	/**
	 * @param StripeAccountUserInterface $accountInterface
	 */
	public function setStripeAccountProperty(StripeAccountUserInterface $accountInterface, $data){
		$ret = \Stripe\Account::update($accountInterface->getStripeAccountId(), $data);
		return $ret;
	}


	/**
	 * @param StripeAccountUserInterface $user
	 * @param $montant
	 * @param null $metaData
	 * @return \Stripe\Payout
	 */
	public function payoutAccount(StripeAccountUserInterface $account, $montant, $options = []){
		$infos = [
			"amount" => $montant,
			"currency" => "eur",
		];
		$infos = array_merge($infos, $options);

		return \Stripe\Payout::create($infos, ["stripe_account" => $account->getStripeAccountId()]);
	}


}

