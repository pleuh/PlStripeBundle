<?php

namespace Pl\StripeBundle\Interfaces;


interface StripeUserInterface{

    /**
     * @return string
     */
    public function getEmail();
    /**
     * @return string
     */
    public function getStripeId();

	/**
     * @param string $stripeId
     * @return mixed
     */
    public function setStripeId($stripeId);


}