<?php

namespace Pl\StripeBundle\Interfaces;


use Stripe\Event;

interface SripeEventCallbackManagerInterface
{
	public function handleEvent(Event $event);
}
