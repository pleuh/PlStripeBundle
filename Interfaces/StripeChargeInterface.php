<?php

namespace Pl\StripeBundle\Interfaces;


interface StripeChargeInterface{

    /**
     * @return string
     */
    public function getStripeChargeId();

	/**
     * @param $statut
     */
    public function setStatut($statut);
    /**
     * @return string
     */
    public function getStatut();


	/**
	 * @return string
	 */
	public function getPaymentIntentId();

	/**
	 * @return StripeChargeInterface
	 */
	public function setPaymentIntentId(string $id);

}