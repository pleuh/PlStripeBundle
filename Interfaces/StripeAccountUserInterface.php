<?php

namespace Pl\StripeBundle\Interfaces;


use App\Interfaces\EntityWithDocumentsInterface;

interface StripeAccountUserInterface extends EntityWithDocumentsInterface{

    /**
     * @return string
     */
    public function getEmailForStripeAccount();

    /**
     * @return string
     */
    public function getStripeAccountId();


	/**
     * @param string $stripeAccountId
     * @return mixed
     */
    public function setStripeAccountId($stripeId);


	/**
	 * Set stripeAccountStatus.
	 *
	 * @param string|null $stripeAccountStatus
	 *
	 * @return StripeAccountUserInterface
	 */
	public function setStripeAccountStatus($stripeAccountStatus = null);

	/**
	 * Get stripeAccountStatus.
	 *
	 * @return string|null
	 */
	public function getStripeAccountStatus();

	/**
	 * @return iterable
	 */
	public function getMetaDataForStripeAccount() :iterable;


	/**
	 * @return iterable
	 */
	public function getAdressForStripeAccount() : iterable;

	/**
	 * @return iterable
	 */
	public function getPersonnalAdressForStripeAccount() : iterable;

	/**
	 * @return string
	 */
	public function getFirstNameForStripeAccount() : string;

	/**
	 * @return string
	 */
	public function getLastNameForStripeAccount() : string;

	/**
	 * @return iterable
	 */
	public function getDobForStripeAccount() : iterable;


	/**
	 * Set stripeError.
	 *
	 * @param string|null $stripeError
	 *
	 * @return StripeAccountUserInterface
	 */
	public function setStripeError($stripeError = null);


	/**
	 * Get stripeError.
	 *
	 * @return string|null
	 */
	public function getStripeError();


}