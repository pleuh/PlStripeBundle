<?php

namespace Pl\StripeBundle\Interfaces;


use App\Entity\Virement;

interface StripeVirementInterface{

	/**
	 * @return string
	 */
	public function getEmail();

	/**
	 * @return string
	 */
	public function getFullName();

	/**
	 * @param $statut
	 */
	public function setStatut($statut);
	/**
	 * @return string
	 */
	public function getStatut();

	/**
	 * @return string
	 */
	public function getStripeSourceId();

	/**
	 * @return StripeVirementInterface
	 * @param string $stripeSourceId
	 */
	public function getStripeIban();

	/**
	 * @return string
	 */
	public function setStripeSourceId($stripeSourceId);

	/**
	 * @return StripeVirementInterface
	 * @param string $stripeIban
	 */
	public function setStripeIban($stripeIban);


	/**
	 * @param string|null $stripeIntentId
	 * @return Virement
	 */
	public function setStripeIntentId($stripeIntentId = null);


	/**
	 * @param string|null $stripeReference
	 * @return Virement
	 */
	public function setStripeReference($stripeReference = null);

	/**
	 * @return string|null
	 */
	public function getStripeIntentId();

	/**
	 * @return string|null
	 */
	public function getStripeReference();

	public function setStripeBic($stripeBic = null);
	public function getStripeBic();
	public function setStripeAccountHolderName($stripeAccountHolderName = null);
	public function getStripeAccountHolderName();



	/**
	 * Set stripePaymentTransfertId.
	 *
	 * @param string|null $stripePaymentTransfertId
	 *
	 * @return Virement
	 */
	public function setStripePaymentTransfertId($stripePaymentTransfertId = null);

	/**
	 * Get stripePaymentTransfertId.
	 *
	 * @return string|null
	 */
	public function getStripePaymentTransfertId();
}
