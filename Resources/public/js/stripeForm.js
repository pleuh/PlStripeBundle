// === requried options
// stripe_api_pk
// emptyUrl
// amount
//
// === unrequired otions
// loadingMessage
// expirationDateErrorMessage
// carteErrorMessage
// errorCallbackOnEl
// iframeHolderEl

$.fn.stripeForm = function(options){
    if(typeof options.stripe_api_pk == "undefined"){
        console.error("in pl/stripeForm.js, vous devez fournir l'option 'stripe_api_pk'");
        return;
    }
    else{
        var stripeApiKey = options.stripe_api_pk;
    }
    if(typeof options.emptyUrl == "undefined"){
        console.error("in pl/stripeForm.js, vous devez fournir l'option 'emptyUrl'");
        return;
    }
    else{
        var emptyUrl = options.emptyUrl;
    }
    if(typeof options.iframeHolderEl != "undefined"){
        var iframeHolderEl = options.iframeHolderEl;
    }
    if(typeof options.amount == "undefined" && options.amountEl == "undefined" ){
        console.error("in pl/stripeForm.js, vous devez fournir l'option 'amount ou un element associe'");
    }
    else if(typeof options.amount != "undefined"){
        var amount = options.amount;
    }
    else if(typeof options.amountEl != "undefined"){
        var amountEl = options.amountEl;
    }

    var loadingMessage = "Chargement";
    var expirationDateErrorMessage = "Votre date d'expiration est incorrecte";
    var carteErrorMessage = "Votre numéro de carte bleue, votre code de vérification ou votre date d'expiration semblent incorrects";

    var errorCallbackOnEl = function(message, el){
        console.error(message, el);
    };

    if(typeof options.loadingMessage != "undefined") {
        loadingMessage = options.loadingMessage;
    }
    if(typeof options.expirationDateErrorMessage != "undefined") {
        expirationDateErrorMessage = options.expirationDateErrorMessage;
    }
    if(typeof options.carteErrorMessage != "undefined") {
        carteErrorMessage = options.expirationDateErrorMessage;
    }
    if(typeof options.errorCallbackOnEl != "undefined") {
        errorCallbackOnEl = options.errorCallbackOnEl;
    }

    var formEl = $(this);
    var submitEl = $(this).find("button[type='submit']");
    beforeLoadingMessage = $(submitEl).html();

    var numberEl, cvxEl, expirationDateEl, sourceEl, sourceChoiceEl, beforeLoadingMessage;
    $(formEl).find("input, select").each(function(){
        if(typeof $(this).attr("id") == "undefined"){
            return;
        }
        if($(this).attr("id").toLowerCase().indexOf("number") > -1){
            numberEl = $(this);
        }
        if($(this).attr("id").toLowerCase().indexOf("expirationdate") > -1){
            expirationDateEl = $(this);
        }
        if($(this).attr("id").toLowerCase().indexOf("cvx") > -1){
            cvxEl = $(this);
        }
        if($(this).attr("id").toLowerCase().indexOf("source") > -1){
            sourceEl = $(this);
        }
        if($(this).attr("id").toLowerCase().indexOf("sourcechoice") > -1){
            sourceChoiceEl = $(this);
        }
    });

    if(typeof numberEl == "undefined"){
        console.error("in pl/stripeForm.js, impossible de trouver l'input du numero de la carte, vérifiez que 'number' est bien présent dans son attribut");
        return;
    }
    if(typeof expirationDateEl == "undefined"){
        console.error("in pl/stripeForm.js, impossible de trouver l'input de la date d'experation de la carte, vérifiez que 'expirationdate' est bien présent dans son attribut");
        return;
    }
    if(typeof cvxEl == "undefined"){
        console.error("in pl/stripeForm.js, impossible de trouver le cvx de la carte, vérifiez que 'cvx' est bien présent dans son attribut");
        return;
    }
    if(typeof sourceEl == "undefined"){
        console.error("in pl/stripeForm.js, impossible de trouver le source de la carte, vérifiez que 'source' est bien présent dans son attribut");
        return;
    }


    if(typeof Stripe == "undefined"){
        console.error("in pl/stripeForm.js, stripe is not defined, verifier que stripe.js est inclu et que la connexion à internet fonctionne");
        return;
    }
    else{
        Stripe.setPublishableKey(stripeApiKey);
    }

    $(formEl).submit(function(){

        if(typeof amount == "undefined" && typeof amountEl != "undefined"){
            amount = parseInt($(amountEl).val() * 100);
        }
        if(typeof amount == "undefined" || amount == 0 || isNaN(amount)){
            amount = undefined;
            return false;
        }

        refreshErrors([$(numberEl), $(expirationDateEl), $(cvxEl)]);
        $(submitEl).prop('disabled', true);
        $(submitEl).html(loadingMessage);


        if($(sourceEl).val() != ""){
            return true;
        }
        else{
            createCard();
        }
        return false;
    });

    if(typeof sourceChoiceEl != undefined){
        function refreshSourceChoice(){
            $(sourceEl).val($(sourceChoiceEl).val());
        }
        $(sourceChoiceEl).change(function(){
            refreshSourceChoice();
        })
        refreshSourceChoice();
    }

    function printBankErrors(message, el){
        refreshErrors();
        errorCallbackOnEl(message, el);
    }

    function refreshErrors(){
        arElementsToClear = [numberEl,  cvxEl, expirationDateEl];
        $(arElementsToClear).each(function(key, val){
            $(val).removeClass("uncorrect");
            $(val).closest("fieldset").find("ul.error").remove();
        });
    }


    function createCard(){
        var date = $(expirationDateEl).val().replace(/[ /,-]+/g, '');
        var resAnneeSurDeuxAns = date.match(/^(\d{2})(\d{2})$/);
        if(typeof resAnneeSurDeuxAns != "undefined" && resAnneeSurDeuxAns != null && resAnneeSurDeuxAns.length > 1){
            date = resAnneeSurDeuxAns[1] + "20" + resAnneeSurDeuxAns[2];
        }
        var res = date.match(/^(\d{2})\d{2}(\d{2})$/);
        if(typeof res == "undefined" || res == null || res.length <= 1){
            printBankErrors(expirationDateErrorMessage, $(expirationDateEl));
            $(submitEl).prop('disabled', false);
            $(submitEl).html(beforeLoadingMessage);
            return;
        }

        Stripe.source.create({
            type: 'card',
            card: {
                number: $(numberEl).val().replace(/[^0-9]+/g, ''),
                cvc: $(cvxEl).val().replace(/[^0-9]+/g, ''),
                exp_month: res[1],
                exp_year: res[2],
            }
        }, stripeResponseHandler);
        return false;
    }


    function stripeResponseHandler(status, response) {
        if (response.error){
            printBankErrors(carteErrorMessage, $(numberEl))
            $(submitEl).prop('disabled', false);
            $(submitEl).html(beforeLoadingMessage);
        } else {
            var source = response.id;
            $(sourceEl).val(source);

            if(typeof response.card.three_d_secure != "undefined" && response.card.three_d_secure == "required"){
                if(typeof iframeHolderEl == "undefined"){
                    console.error("in pl/stripeForm.js, vous devez fournir l'option 'iframeHolderEl' pour pouvoir garantier les paiements 3Dsecure");
                    return;
                }
                Stripe.source.create({
                    type: 'three_d_secure',
                    amount: amount,
                    currency: "eur",
                    three_d_secure: {
                        card: source
                    },
                    redirect: {
                        return_url: emptyUrl
                    }
                }, stripe3DSecureResponseHandler);
            }
            else{
                $(formEl).get(0).submit();
            }
        }
    }
    function stripe3DSecureResponseHandler(status, response) {
        $('<iframe />');
        $('<iframe />', {
            id: '3dsecureIframe',
            style: 'width: 100%; height: 400px;',
            src: response.redirect.url
        }).appendTo(iframeHolderEl);

        Stripe.source.poll(
            response.id,
            response.client_secret,
            function(status, source) {
                if(source.status == "chargeable"){
                    $(sourceEl).val(source.id);
                    $(formEl).get(0).submit();
                }
            });

    }

}